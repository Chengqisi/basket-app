import Vue from 'vue'
import App from './App'
import axios from "axios"
import util from './utils/utils'
// import "@vant/weapp/lib/
Vue.config.productionTip = false
Vue.prototype.$axios = axios
App.mpType = 'app'
Vue.prototype.$compress=util.compress

Vue.prototype.$date = function (d) {
	var year = d.getFullYear();
	var month = d.getMonth() + 1; // 记得当前月是要+1的
	month = month < 10 ? ("0" + month) : month;
	var dt = d.getDate();
	dt = dt < 10 ? ("0" + dt) : dt;
	var h = d.getHours()
	var m = d.getMinutes()
	var s = d.getSeconds()
	console.log(h, m, s);
	var today = year + "-" + month + "-" + dt + "  " + `${h > 9 ? `${h}` : `0${h}`}` + ":" + `${m > 9 ? `${m}` : `0${m}`}` + ":" + `${s > 10 ? `${s}` : `0${s}`}`;
	return today;
}


const app = new Vue({
	...App
})
app.$mount()
